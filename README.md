<h1>Setup Environment</h1>

Install Nodejs(8.12):
https://nodejs.org/en/

Install Postman:
https://www.getpostman.com/


check if installed properly:
open cmd
type "node -v"

<h1>Run the api</h1> 

open the command prompt and navigate to the project folder.

type "nodemon server.js" or "node server.js"
open browser and in url bar type "http://localhost:3000/products" or "http://localhost:3000/orders"



const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Order = require('../models/order');
const Product = require('../models/product');


router.get('/',(req,res,next)=>{
    Order.find().select('product quantity _id orderDate').populate('product','name').exec()
                 .then(docs=>{
                     const response = {
                         count:docs.length,
                         orders:docs
                     };
                    // console.log(docs);
                     res.status(200).json(response);
                 })
                 .catch(err=>{
                     console.log(err);
                     res.status(500).json({
                        error:err
                     });
                 }) 
});


router.post('/',(req,res,next)=>{
    Product.findById(req.body.productId)
           .then(product=>{
               if(!product){
                   return res.status(404).json({
                       message:"Product not found"
                   })
               }

            var now  = new Date();
            const order = new Order({
                _id : mongoose.Types.ObjectId(),
                quantity: req.body.quantity,
                product: req.body.productId,
                orderDate: now.toUTCString()
            }); 
        
           return order.save().then(result =>{
                 res.status(201).json({
                    message:"Order Created",
                    createdOrder:{
                        id: result._id,
                        product:result.product,
                        quantity:result.quantity,
                        date: result.orderDate
                    }
                 });
               }
           )     
           })
           .catch(err=>{
               res.status(500).json({
               // message:"Product not found",
                error:err
               });
           });    
});

router.get('/:orderId',(req,res,next)=>{
    Order.findById(req.params.orderId).populate('product')
         .exec()
         .then(order =>{
             if(!order){
                 res.status(404).json({
                     message:"Order not found"
                 })
             }
             res.status(200).json({
                 order: order
             })
         })
         .catch(err=>{
             res.status(500).json({
                 error:err
             })
         })
});

router.delete('/:orderId',(req,res,next)=>{
    Order.remove({_id:req.params.orderId})
         .exec()
         .then(result=>{
             res.status(200).json({
                 message:"Order deleted"
             })
         })
         .catch(err=>{
             res.status(500).json({
                 message:"Order Not Deleted",
                 error:err
             })
            } 
         )       
});

module.exports = router;
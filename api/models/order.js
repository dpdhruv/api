const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    product:{ type: mongoose.Schema.Types.ObjectId,ref:'Product',required:true},
    quantity:{type:Number , default:1},
    orderDate: String
});

module.exports = mongoose.model('Order',productSchema);